<?php
class RPiData {
    private $map = array();

    public function put($key, $value) {
        $this->map[$key] = $value;
    }

    public function fromString($string) {
        foreach (explode("\n", $string) as $key => $value) {
            if (trim($value) === "") {
                continue;
            }
            $keyValue = explode("=", $value);
            $this->put($keyValue[0], $keyValue[1]);
        }
    }

    public function toString() {
        $result = "";
        foreach ($this->map as $key => $value) {
            $result .= $key . "=" . $value . "\n";
        }
        return $result;
    }

    public function toJSON() {
        return json_encode($this->map);
    }
}
?>
