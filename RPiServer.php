<?php
include "RPiData.php";

set_error_handler("errorHandler");
set_exception_handler('exceptionHandler');

$SETTINGS = loadSettings();
$RPI_FILE_NAME = $SETTINGS["rpi_status_file"];
$rpiData = new RPiData();
$OPERATION = $_POST["operation"];



switch ($OPERATION) {
case "put":
    foreach ($_POST as $key => $value) {
        $key = htmlspecialchars(trim($key));
        $value = htmlspecialchars(trim($value));
        if ($key === "operation") {
            continue;
        } else if (in_array($key, $SETTINGS["keys"], true)) {
            $rpiData->put($key, $value);
        }
    }
    $rpiString = $rpiData->toString();
    file_put_contents($RPI_FILE_NAME, $rpiString);
    if (array_key_exists("email_to", $SETTINGS)
            && array_key_exists("email_from", $SETTINGS)) {
        $email = $SETTINGS["email_to"];
        $message = wordwrap($rpiString);
        $headers = "From: " . $SETTINGS["email_from"] . "\r\n" .
                "Reply-To: " . $SETTINGS["email_from"] . "\r\n" .
                "X-Mailer: PHP/" . phpversion();
        mail($email, "RPi Info", $message, $headers);
    }
    echo createResponse("SUCCESS", $rpiString);
    break;
case "get":
    $rpiFileContent = file_get_contents($RPI_FILE_NAME);
    $rpiData->fromString($rpiFileContent);
    echo $rpiData->toJSON();
    break;
default:
    throw new Exception("Unknown operation type: \"" . $OPERATION . "\"");
}



function loadSettings($fileName="settings.cfg") {
    $settings = array();
    foreach (explode("\n", file_get_contents($fileName)) as $key => $value) {
        if (trim($value) === "") {
            continue;
        }
        $map = explode("=", $value);
        $settingName = trim($map[0]);
        $settingValue = ($settingName === "keys")
                ? explode(",", trim($map[1]))
                : trim($map[1]);
        $settings[$settingName] = $settingValue;
    }
    return $settings;
}

function createResponse($status, $message) {
    $fileName = "./html/";
    $replaceString = "";
    if ($status === "ERROR") {
        $fileName .= "error.html";
        $replaceString = "\${ERROR_MESSAGE}";
    } else if ($status === "SUCCESS") {
        $fileName .= "success.html";
        $replaceString = "\${RPI_STATUS}";
    }
    $message = str_replace("\n", "<br/>", $message);
    $responseContent = file_get_contents($fileName);
    return str_replace($replaceString, $message, $responseContent);
}

function errorHandler($errno, $errstr, $errfile, $errline) {
    $exception = new ErrorException($errstr, $errno, 0, $errfile, $errline);
    exceptionHandler($exception);
}

function exceptionHandler($exception) {
    $message = "<b>Message:</b> " . $exception->getMessage() . "<br/>"
            ."<b>File:</b> " . $exception->getFile() . "<br/>" 
            . "<b>Line:</b> " . $exception->getLine() . "<br/>"
            . $exception->getTraceAsString();
    echo createResponse("ERROR", $message);
    die();
}
?>
